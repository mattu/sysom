import React, {useState,useEffect} from 'react';
import ProTable from '@ant-design/pro-table';
import {PauseOutlined, RedoOutlined,FileTextOutlined} from "@ant-design/icons";
import {queryStopAssess, queryRetryAssess,queryDeleteAssess} from '../../../service';
import {Progress, Button, Modal, message, Tooltip} from 'antd';
import './AssessList.less';
import { useIntl, FormattedMessage } from 'umi';


const AssessList = React.forwardRef((props, ref) => {
  const {assessList,getList} = props;
  const [loading,setLoading] = useState(true);
  const intl = useIntl();

  useEffect(()=>{
    new Promise(async(resolve) => {
      await getList();
      resolve();
    }).then(()=>{
      setLoading(false);
    });
    // 轮询评估记录
    const timer = setInterval(async () => {
      Promise.all([
        getList()
      ]).catch((error)=>{
        console.log(error,'error')
      })
    }, 5000);
    return () => clearInterval(timer);
  },[])

  const columns = [
    {
      title: <FormattedMessage id="pages.migrate.machinename" defaultMessage="Machine name" />,
      dataIndex: 'hostname',
    },
    {
      title: 'IP',
      dataIndex: 'ip',
    },
    {
      title: <FormattedMessage id="pages.migrate.sourcesystem" defaultMessage="Source operating system" />,
      dataIndex: 'old_ver',
    },
    {
      title: <FormattedMessage id="pages.migrate.targetsystem" defaultMessage="Target operating system" />,
      dataIndex: 'new_ver',
    },
    {
      title: <FormattedMessage id="pages.migrate.evaluateprogress" defaultMessage="Evaluate progress" />,
      dataIndex: 'rate',
      filters: false,
      renderText: (_, r) => (
        <Progress
          percent={r.rate || 0}
          className={
            r.rate === 100
              ? 'numGreen'
              : r.rate > 0 && r.rate < 99
              ? 'numRed'
              : 'numZero'
          }
          size='small'
          steps={10}
          format={(percent) => `${percent}%`}
          strokeColor={r.rate === 100 ? '#52C41A' : (r.rate > 0 && r.rate < 99 ? '#FF4D4F' : '#999999')}
        />
      ),
    },
    {
      title: <FormattedMessage id="pages.migrate.evaluationstatus" defaultMessage="Evaluation status" />,
      dataIndex: 'status',
      render: (t, r) => {
        switch (r.status){
            case 'running':
              return (
                <Tooltip trigger="hover" placement="topLeft" title={r.detail ? r.detail : ''}>
                  <span style={{fontSize: 13,cursor: 'pointer',color: '#FCC00B'}}><FormattedMessage id="pages.migrate.underevaluation" defaultMessage="Under evaluation" /></span>
                </Tooltip>
              );
            case 'success':
              return (
                <Tooltip trigger="hover" placement="topLeft" title={r.detail ? r.detail : ''}>
                  <span style={{fontSize: 13,cursor: 'pointer',color: '#52C41A'}}><FormattedMessage id="pages.migrate.evaluationcompleted" defaultMessage="Evaluation completed" /></span>
                </Tooltip>
              );
            case 'fail':
              return (
                <Tooltip trigger="hover" placement="topLeft" title={r.detail ? r.detail : ''}>
                  <span style={{fontSize: 13,cursor: 'pointer',color: '#FF4D4F'}}><FormattedMessage id="pages.migrate.evaluationfailure" defaultMessage="Evaluation failure" /></span>
                </Tooltip>
              );
            case 'stop':
              return (
                <Tooltip trigger="hover" placement="topLeft" title={r.detail ? r.detail : ''}>
                  <span style={{fontSize: 13,cursor: 'pointer',color: '#FF4D4F'}}><FormattedMessage id="pages.migrate.evaluationstop" defaultMessage="Evaluation stop" /></span>
                </Tooltip>
              );
            default: return r.status;
        }
      },
    },
    {
      title: <FormattedMessage id="pages.migrate.evaluationtime" defaultMessage="Evaluation time" />,
      dataIndex: 'created_at',
    },
    // 评估中：可以停止，其他不能；不能重试，其他能。
    {
      title: <FormattedMessage id="pages.migrate.operation" defaultMessage="Operation" />,
      dataIndex: 'option',
      valueType: 'option',
      width: 300,
      render: (_, record) => (
        <div className='option'>
          <Button type='text' className='optionBtn' disabled={record.status==='running'?false:true} onClick={() => onStop(record.id)}>
            <PauseOutlined />
            <span><FormattedMessage id="pages.migrate.stop" defaultMessage="Stop" /></span>
          </Button>
          <Button type='text' className='optionBtn' disabled={record.status==='running'?true:false} onClick={() => onRetry(record.id)}>
            <RedoOutlined />
            <span><FormattedMessage id="'pages.migrate.retry" defaultMessage="Retry" /></span>
          </Button>
          <Button type='text' className='optionBtn' disabled={record.status==='running'?true:false} onClick={() => onDelete(record.id)}>
            <FileTextOutlined />
            <span style={{marginLeft: '8px'}}><FormattedMessage id="pages.migrate.delete" defaultMessage="Delete" /></span>
          </Button>
          <Button type='text' className='optionBtn'>
            <a key={record.id} href={'/migrate/report/'+record.id+'?ip='+record.ip+'&old_ver='+record.old_ver+'&new_ver='+record.new_ver}>
              <FileTextOutlined />
              <span style={{marginLeft: '8px'}}><FormattedMessage id="pages.migrate.viewreport" defaultMessage="Viewreport" /></span>
            </a>
          </Button>
        </div>
      ),
    }
  ];

  const onStop = (id) => {
    Modal.confirm({
      title: (
        <span style={{ fontWeight: 'normal', fontSize: 14 }}>
          <FormattedMessage id="pages.migrate.areyoustop" defaultMessage="Are you sure you want to stop?" />
        </span>
      ),
      okText: <FormattedMessage id="pages.migrate.ok" defaultMessage="OK" />,
      cancelText: <FormattedMessage id="pages.migrate.cancel" defaultMessage="Cancel" />,
      onOk: async () => {
        const destroyHide = message.loading('正在停止...', 0);
        try {
          const { code, msg } = await queryStopAssess({
            id,
          });
          if (code === 200) {
            getList();
            message.success('停止成功');
            return true;
          }
          message.error(msg);
          return false;
        } catch (error) {
          console.log(error,'error')
          return false;
        } finally {
          destroyHide();
        }
      },
    });
  }

  const onRetry = (id) => {
    Modal.confirm({
      title: (
        <span style={{ fontWeight: 'normal', fontSize: 14 }}>
          <FormattedMessage id="pages.migrate.areyouretry" defaultMessage="Are you sure you want to retry?" />
        </span>
      ),
      okText: '确定',
      cancelText: '取消',
      onOk: async () => {
        const destroyHide = message.loading('正在重试...', 0);
        try {
          const { code, msg } = await queryRetryAssess({id});
          if (code === 200) {
            getList();
            message.success('重试成功');
            return true;
          }
          message.error(msg);
          return false;
        } catch (error) {
          console.log(error,'error')
          return false;
        } finally {
          destroyHide();
        }
      },
    });
  }

  const onDelete = (id) => {
    Modal.confirm({
      title: (
        <span style={{ fontWeight: 'normal', fontSize: 14 }}>
          <FormattedMessage id="pages.migrate.areyoudelete" defaultMessage="Are you sure you want to delete it?" />
        </span>
      ),
      okText: '确定',
      cancelText: '取消',
      onOk: async () => {
        const destroyHide = message.loading('正在删除...', 0);
        try {
          const { code, msg } = await queryDeleteAssess({id});
          if (code === 200) {
            getList();
            message.success('删除成功');
            return true;
          }
          message.error(msg);
          return false;
        } catch (error) {
          console.log(error,'error')
          return false;
        } finally {
          destroyHide();
        }
      },
    });
  }

  return (
    <ProTable
      tableClassName='assessList'
      headerTitle={props.headerTitle}
      actionRef={ref}
      params={props.params}
      rowKey='id'
      dataSource={assessList}
      columns={columns}
      pagination={props.pagination}
      search={false}
      loading={loading}
    />
  );
});

export default AssessList;

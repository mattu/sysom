import React  from "react";
import ProTable from "@ant-design/pro-table";
import { useIntl, FormattedMessage } from 'umi';

const DiagnoTableList = React.forwardRef((props, ref) => {
  const intl = useIntl();
  const columns = [
    {
      title: <FormattedMessage id="pages.diagnose.checkitem" defaultMessage="Check item" />,
      dataIndex: "item",
      valueType: "textarea",
      width: 200,
    },
    {
      title: <FormattedMessage id="pages.diagnose.state" defaultMessage="State" />,
      dataIndex: 'level',
      width: 100,
      valueEnum: {
        none: { text: <FormattedMessage id="pages.diagnose.normal" defaultMessage="Normal" />, status: 'Success' },
        info: { text: <FormattedMessage id="pages.diagnose.prompt" defaultMessage="Prompt" />, status: 'Warning' },
        warning: { text: <FormattedMessage id="pages.diagnose.alarm" defaultMessage="Alarm" />, status: 'Warning' },
        error: { text: <FormattedMessage id="pages.diagnose.generalerror" defaultMessage="General error" />, status: 'Error' },
        critical: { text: <FormattedMessage id="pages.diagnose.seriouserror" defaultMessage="Serious error" />, status: 'Error' },
        fatal: { text: <FormattedMessage id="pages.diagnose.fatalerror" defaultMessage="Fatal error" />, status: 'Error' },
      },
    },
    {
      title: <FormattedMessage id="pages.diagnose.inspectionresult" defaultMessage="Inspection result" />,
      dataIndex: "summary",
      valueType: "textarea",
      ellipsis: true,
    },
    {
      title: <FormattedMessage id="pages.diagnose.operation" defaultMessage="Operation" />,
      dataIndex: "option",
      valueType: "option",
      width: 200,
      render: (_, record) => {
        if (record.level != "none") {
          return (
            <a key="showError" onClick={() => {
              props?.onRepair?.(record)
            }}><FormattedMessage id="pages.diagnose.automaticrepair" defaultMessage="Automatic repair" /></a>
          )
        }
        else {
          return (<span></span>);
        }
      },
    }
  ];

  //  defaultExpandAllRows={true}
  return (
    <ProTable
      headerTitle={props.headerTitle}
      actionRef={ref}
      rowKey="key"
      dataSource={props.data}
      columns={columns}
      pagination={false}
      search={false}
    />
  );
});

export default DiagnoTableList;

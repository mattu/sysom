import ProForm, { ProFormDigit, ProFormText, ProFormSelect } from '@ant-design/pro-form';
import { Button } from 'antd';
import ProCard from '@ant-design/pro-card';
import { useRequest, FormattedMessage, useIntl } from 'umi';
import { useEffect, useState, useRef } from 'react';
import { postTask } from '../service';
import { getHost } from '@/pages/host/service'

const TaskFrom = (props) => {
  const taskForm = props.taskForm
  const serviceName = props.serviceName
  const formRef = useRef();
  const [hostList, setHostList] = useState([]);
  const intl = useIntl();


  const { loading, error, run } = useRequest(postTask, {
    manual: true,
    onSuccess: (result, params) => {
      props?.onSuccess?.(result, params)
    },
  });

  useEffect(async ()=> {
    const { data } = await getHost()
    let list = []
    data.map((item) => {
        list.push({"value": item.ip, "label": item.ip})
    })
    setHostList(list)
  }, [])

  const onChange = (value) => {
    if (value) {
      formRef?.current?.setFieldsValue({'instance': value})
    }
  };
  const onSearch = (value) => {
      if (value) {
        formRef?.current?.setFieldsValue({'instance': value})
      }
  };

  return (
    <ProCard>
      <ProForm
        formRef={formRef}
        onFinish={async (values) => {
          run(values) 
        }}
        submitter={{
          submitButtonProps: { style: { display: 'none' } },
          resetButtonProps: { style: { display: 'none' } }
        }}
        layout={"horizontal"}
        autoFocusFirstInput
      >
        <ProFormText
          name={"service_name"}
          initialValue={serviceName}
          hidden={true}
        />
        <ProForm.Group>
          {
            taskForm?.map(formItem => {

              if (formItem.type == "text") {
                return (< ProFormText
                  key={formItem.name}
                  name={formItem.name}
                  label={formItem.label}
                  initialValue={formItem.initialValue}
                  tooltip={formItem.tooltips}
                />);
              } else if (formItem.type == "digit") {
                return (< ProFormDigit
                  key={formItem.name}
                  name={formItem.name}
                  label={formItem.label}
                  initialValue={formItem.initialValue}
                  tooltip={formItem.tooltips}
                />);
              } else if (formItem.type == "select") {
                return (
                  < ProFormSelect
                    key={formItem.name}
                    name={formItem.name}
                    label={formItem.label}
                    initialValue={formItem.initialValue}
                    tooltip={formItem.tooltips}
                    options={formItem.options}
                  />);
              } else if (formItem.type == "select_host") {
                return (<ProFormSelect
                  name={"instance"}
                  width="md"
                  label={intl.formatMessage({
                      id: 'pages.diagnose.instanceIP',
                      defaultMessage: 'Instance IP',
                  })}
                  rules={[
                      { required: true, message: 'Please input your instance!' },
                  ]}
                  options={hostList}
                  fieldProps={{
                      "allowClear": true,
                      "showSearch": true,
                      "onChange": onChange,
                      "onSearch": onSearch
                  }}
                  placeholder="Please select or input instance!"
              />)

              }
            })
          }
          <Button type="primary" htmlType="submit" loading={loading}><FormattedMessage id="pages.diagnose.startdiagnosis" defaultMessage="Start diagnosis" /></Button>
          <Button type="primary" loading={loading} onClick={() => {
            props?.onOfflineLoad?.()
          }}>
            {
              intl.formatMessage({
                id: 'pages.diagnose.offline_import.btn',
                defaultMessage: 'Offline import',
              })
            }
          </Button>
        </ProForm.Group>
      </ProForm>
    </ProCard>
  )
}

export default TaskFrom

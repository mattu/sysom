import { PageContainer } from '@ant-design/pro-layout';
import ProForm, { ProFormSelect, ProFormText, ProFormDigit } from '@ant-design/pro-form';
import { message, Button, Row, Col, Descriptions } from 'antd';
import ProCard from "@ant-design/pro-card";
import { useIntl, FormattedMessage } from 'umi';

const VmcoreConfigTopCon = (props) => {
  const intl = useIntl();
  const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 14 },
  }      
  return (
    <ProCard>
      <Descriptions column={1} contentStyle={{ color:'#999' }} labelStyle={{ textAlign: 'right', width:'15.5%', height:'30px',display:'block' }}>
        <Descriptions.Item label={intl.formatMessage({
          id: 'pages.vmcore.warehousename',
          defaultMessage: 'Warehouse name',
        })}>{props.data.name}</Descriptions.Item>
        <Descriptions.Item label={intl.formatMessage({
          id: 'pages.vmcore.NFSserverIP',
          defaultMessage: 'NFS server IP address',
        })}>{props.data.server_host}</Descriptions.Item>
        <Descriptions.Item label={intl.formatMessage({
          id: 'pages.vmcore.mountdirectory',
          defaultMessage: 'Mount directory',
        })}>{props.data.mount_point}</Descriptions.Item>
        <Descriptions.Item label={intl.formatMessage({
          id: 'pages.vmcore.storageduration',
          defaultMessage: 'Storage duration',
        })}>{props.data.days}</Descriptions.Item>
      </Descriptions>
    </ProCard>
  );
};

export default VmcoreConfigTopCon;
